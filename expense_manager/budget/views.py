from django.http import HttpResponse
from django.shortcuts import render, redirect
from .models import Expense


# Create your views here.


def add_expense(request):
    if request.method == 'GET':
        return render(request, 'form.html')
    if request.method == 'POST':
        name = request.POST.get('name')
        amount = request.POST.get('amount')
        exp = Expense.objects.create(name=name, amount=amount)
        return redirect('/expenses/')


def get_all_expenses(request):
    if request.method == 'POST':
        return HttpResponse('method not allowed', status=405)
    if request.method == 'GET':
        expenses = Expense.objects.all()
        # name = request.GET.get('name')
        # expenses = Expense.objects.filter(name=name) or id=''
        # id = request.GET.get('id')
        # expenses = Expense.objects.get(id=id)
        return render(request, 'expenses.html', context={'expenses': expenses})
